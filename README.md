Role Name
=========

AutoLogic's Role for managing JenkinsCI installations on UNIX systems.

License
-------

BSD

Author Information
------------------

- Michael Crilly
- AutoLogic Technology Ltd
- http://www.mcrilly.me/
